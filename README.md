<h1>mensajito.mx</h1>
              <h5>
                Mensajito, surge de la idea de crear puentes de comunicación libre, una opción económica para quienes quieran desarrollar una estación de radio por internet o una segunda línea de transmisión para quienes ya hacen radio por frecuencia.
              </h5>
              <h5>
                Es gratuito, libre y modificable. busca generar comunidad a partir de contenidos auditivos dando la posibilidad de interconectar personas e intercambiar información, así como generar espacios libres de reflexión sobre tecnología.
              </h5>
              <h5>
                Mensajito es una colaboración creativa sin fines de lucro que busca crear oportunidades para comunidades de artistas, productores musicales, radios comunitarias o personas que quieran experimentar con el audio.
              </h5>
              <h5>
                Proyecto en colaboración con <a href="http://holadiego.com">Diego Aguirre</a>
              </h5>
              <h5><a href="http://mensajito.mx">mensajito.mx</a></h5>